#!/bin/bash
##LOAD .profile in case of cron
. ~/.profile
##INIT BASICS
. ~/.backupdomains.conf ## LOAD CONFIG FROM FILE
#######################################
##~/.backupdomains.conf should contain
#| BACKUP_DEST=~/backup/
#| DOMAINS=/domains
### and if you want do mongodump collections as well 
#| CONFIGTABLE=system_settings
#| CONFIGITEM=blabla_database_settings
#| OCTOBERONLY=true
###BACKUP ONLY OCTOBERCMS FILES
#| KEEPDAYS=7  
###( backup older than 7 days will be deleted , defaults to 7)
#| INTERVAL=12
### BACKUP  INTERVAL IN HOURS ( 168 for 1 week, 5124 for ~1 month)
### DEFAULT INTERVAL IS 24H 
#######################################
###
###
#the .env files need smth like
#|DB_USERNAME=user
#|DB_CONNECTION=mysql

MONGODUMP=$(which mongodump || echo "NO MONGODUMP")
MYSQLDUMP=$(which mysqldump || (echo "INSTALL MYSQLDUMP!";exit 1))
TIMESTAMP=$(date -u +%Y-%m-%d-%H-%M)


test -e $BACKUP_DEST || (echo "NO BACKUP DIR" ; mkdir $BACKUP_DEST)
mkdir -p $BACKUP_DEST/$TIMESTAMP
newest=$(for i in $(find $BACKUP_DEST/ -type f -name "[1-9][0-9][0-9][0-9]-[0-9][0-9]-*.tgz"  );do date -u +%s -r $i;done|sort -n|tail -n1);
if  [[  -z  $newest  ]];then newest=666666 ; fi 
age_newest_hours=$( expr $(expr  $(date -u +%s) - $newest ) / 60 / 60  ); age_newest_days=$(expr $age_newest_hours / 24 );
if  [  -z $INTERVAL  ];then INTERVAL=24 ; fi 
if  [  -z $KEEPDAYS  ];then KEEPDAYS=7 ; fi 

echo $INTERVAL "@" $newest "->" $age_newest_hours
if [ "$age_newest_hours" -ge "$INTERVAL" ] ; then
	echo "custom backup 0.0.0.1 - Saving "$DOMAINS" to "$BACKUP_DEST" with "$MONGODUMP" and "$MYSQLDUMP
	find $DOMAINS/ -maxdepth 1 -not \( -path $DOMAINS"/*experimental" -prune \)  -type d  |grep -v -e $DOMAINS/"$" | while read DOMAINDIR ; do
		if [ "$NO_DOCROOT" == "true" ]; then
			DOMAINALIAS=${DOMAINDIR/$DOMAINS/}
			echo "NOT SAVING $(basename $DOMAINALIAS) DOCROOT because NO_DOCROOT is true"
		else
			echo -ne "→→SAVING DOMAIN DIRECTORY $DOMAINDIR→\t"; 
			DOMAINALIAS=${DOMAINDIR/$DOMAINS/}
			if [ "$OCTOBER_ONLY" == "true" ]; then
				tar czf $BACKUP_DEST/$TIMESTAMP/$DOMAINALIAS".tar.gz" $(find $DOMAINDIR/{live,test}/{themes,storage}/ -maxdepth 1 -mindepth 1 -not -path "*storage/temp"  -not -path "*storage/logs" -not -path "*storage/session" -not -path "*storage/cache" 2>/dev/null) 2>/dev/null ;
			else
				tar czf $BACKUP_DEST/$TIMESTAMP/$DOMAINALIAS".tar.gz" $DOMAINDIR/ 2>/dev/null ;
			fi
		fi
		find $DOMAINDIR -name "\.env" -type f -exec dirname {} \; |while read dir ;do 
			envfile=$dir/.env ;
			echo -n "$dir described by $envfile →" ;
			##GET DB CREDENTIALS
			{
			DBCON=$(grep ^DB_CONNECTION $envfile || echo DB_CONNECTION=mysql);DBCON=$(echo $DBCON|cut -d= -f2);
			DBNAM=$(grep ^DB_DATABASE $envfile );DBNAM=$(echo $DBNAM|cut -d= -f2);
			DBUSR=$(grep ^DB_USERNAME $envfile );DBUSR=$(echo $DBUSR|cut -d= -f2);
			DBPAS=$(grep ^DB_PASSWORD $envfile );DBPAS=$(echo $DBPAS|cut -d= -f2);
			if  [[ !  -z  $DBCON  ]] && [[ !  -z  $DBNAM  ]] && [[ !  -z  $DBPAS  ]] && [[ !  -z  $DBUSR  ]];then 
				echo " MYSQL: "
			 	$MYSQLDUMP -u$DBUSR -p$DBPAS $DBNAM --single-transaction > $BACKUP_DEST/$TIMESTAMP/$DOMAINALIAS"-MYSQL-"$DBNAM".sql" 
				#ls -lh1 $BACKUP_DEST/$TIMESTAMP/$DOMAINALIAS"-MYSQL-"$DBNAM".sql"
				###GET MONGO Collection
				MONGODB=$(mysql -u$DBUSR -p$DBPAS --batch --silent -e "select value from $CONFIGTABLE where item = '$CONFIGITEM' ;" --database=$DBNAM 2>/dev/null |sed 's/","/",\n"/g'|grep database|cut -d\" -f4) 
				if  [[ !  -z  $MONGODB  ]];then
					echo -n " MONGO: "$MONGODB" →$BACKUP_DEST/$TIMESTAMP/$DOMAINALIAS  "
					#mongodump --db $MONGODB -collection $MONGOCOL --out
					# mongodump --quiet --db $MONGODB --out $BACKUP_DEST/$TIMESTAMP/$DOMAINALIAS"-MONGODB-"$MONGODB ;
					mongodump --quiet -u $DBUSR -p$DBPAS --db $MONGODB --out $BACKUP_DEST/$TIMESTAMP/$DOMAINALIAS"-MONGODB-"$MONGODB ;
					#ls -lh1 $BACKUP_DEST/$TIMESTAMP/$DOMAINALIAS"-MONGODB-"$MONGODB ;
				else
					echo -n " NO MONGODB  "
				
				fi
			else 
				echo -n " NO DB CONFIGURED IN .env "
			fi
			}
		done ; echo DONE ;
	done
	
	echo -n "→→SAVING LETSENCRYPT" ;
	test -d /etc/letsencrypt && tar czf $BACKUP_DEST/$TIMESTAMP/letsencrypt.tar.gz /etc/letsencrypt
	find $BACKUP_DEST/$TIMESTAMP/ -type f -exec md5sum {} \; > $BACKUP_DEST/$TIMESTAMP/md5sum
	tar czf $BACKUP_DEST/$TIMESTAMP.tgz $BACKUP_DEST/$TIMESTAMP
	find $BACKUP_DEST/$TIMESTAMP -delete
	du -h -s $BACKUP_DEST/$TIMESTAMP.tgz
fi

	if  [[ !  -z  $KEEPDAYS  ]];then
		find $BACKUP_DEST/ -name "[1-9][0-9][0-9][0-9]-[0-9][0-9]-*.tgz" -mtime +$KEEPDAYS -delete;
	fi
	find $BACKUP_DEST/$TIMESTAMP -type d -delete
echo FINISHED
